﻿using System;

namespace c__practice
{
    class Program
    {
        static void Main(string[] args)
        {
            int A, B, SUM;
            Console.WriteLine("Enter Two Integers");
            A = int.Parse(Console.ReadLine());
            B = int.Parse(Console.ReadLine());
            SUM = A + B;
            Console.WriteLine("Sum of {0} and {1} is {2}", A, B, SUM);
            Console.Read();
        }
    }
}
